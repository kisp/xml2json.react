import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { parseString } from 'xml2js';


class App extends Component {

  state = {
    json: {}
  }

  onFileLoad = (e) => {
    parseString(e.target.result, (err, result) =>
      this.setState({ json: result }));
  }

  fileChange = (e) => {
    const reader = new FileReader();
    reader.addEventListener('load', this.onFileLoad);
    reader.readAsText(e.target.files[0]);
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Please select an XML file</h1>
        </header>
        <p className="App-intro">
          <input type="file"
            onChange={this.fileChange}
            accept=".xml"
            dir="~"
            title="Select an XML file!"
          ></input>
        </p>
        <p>
          <textarea style={{width:'80%', height:'600px'}}
          value={JSON.stringify(this.state.json, null, 2)}
          onChange={()=>{}} />
        </p>
      </div>
    );
  }
}

export default App;
